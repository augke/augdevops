#!/bin/bash

if ! hash python; then
    echo "Python is required to run some of these tests"
    exit 1
fi

pyver=$(python -V 2>&1 | sed 's/.* \([0-9]\).\([0-9]\).*/\1\2/')
if [[ "$pyver" -lt "27" ||  "$pyver" -gt "30" ]]
then
    echo "This script requires Python 2.7.X installed found: " "$pyver"
fi